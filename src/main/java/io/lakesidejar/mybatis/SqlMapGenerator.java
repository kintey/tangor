package io.lakesidejar.mybatis;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.exception.InvalidConfigurationException;
import org.mybatis.generator.exception.XMLParserException;
import org.mybatis.generator.internal.DefaultShellCallback;

public class SqlMapGenerator {

    public static void main(String[] args) {
        // String filePath = "C:\\Users\\Zero\\workspace\\tools\\src\\test\\resources\\maison-sql-map.xml";
        String filePath = "src\\test\\resources\\maison-sql-map.xml";
        try {
            SqlMapGenerator generator = new SqlMapGenerator();
            generator.start(filePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void start(String filePath) throws IOException, XMLParserException, InvalidConfigurationException, SQLException, InterruptedException {

        List<String> warnings = new ArrayList<String>();
        boolean overwrite = true;

        File configFile = new File(filePath); 
        ConfigurationParser cp = new ConfigurationParser(warnings);
        Configuration config = cp.parseConfiguration(configFile);

        DefaultShellCallback callback = new DefaultShellCallback(overwrite);
        MyBatisGenerator myBatisGenerator = new MyBatisGenerator(config, callback, warnings);
        myBatisGenerator.generate(null);

    }

}
