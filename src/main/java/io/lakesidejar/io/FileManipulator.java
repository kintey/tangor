package io.lakesidejar.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileManipulator {

    public static List<String> getAbsolutePaths(String directory, String extension) {

        List<String> files = new ArrayList<String>();
        File dir = new File(directory);

        for (File file : dir.listFiles()) {
            if (file.isDirectory()) {
                List<String> ls = getAbsolutePaths(file.getPath(), extension);

                for (String f : ls) {
                    files.add(f);
                }
            }

            if (extension == null) {
                if (!file.isDirectory())
                    files.add(file.getAbsolutePath());
            } else {
                if (file.getName().endsWith(extension))
                    files.add(file.getAbsolutePath());
            }
        }

        return files;
    }

    public static List<String> getAbsolutePaths(String directory) {

        return getAbsolutePaths(directory, null);

    }

    public static boolean isBinary(String path) throws IOException {

        boolean binary = false;
        File file = new File(path);
        FileInputStream fis = new FileInputStream(file);
        for (long i = 0, len = file.length(); i < len; i++) {
            int t = fis.read();
            if (t < 32 && t != 9 && t != 10 && t != 13) {
                binary = true;
                break;
            }
        }
        fis.close();

        return binary;
    }
}
