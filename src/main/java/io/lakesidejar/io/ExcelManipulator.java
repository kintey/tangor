package io.lakesidejar.io;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelManipulator {

    public static Object[][] read(String filePath) throws InvalidFormatException, IOException {

        return read(filePath, 0, null);

    }

    public static Object[][] read(String filePath, int index) throws InvalidFormatException, IOException {

        return read(filePath, index, null);

    }

    public static Object[][] read(String filePath, String name) throws InvalidFormatException, IOException {

        return read(filePath, 0, name);

    }

    public static Object[][] read(String filePath, int index, String name) throws InvalidFormatException, IOException {

        FileInputStream fis = new FileInputStream(filePath);
        Workbook workbook = WorkbookFactory.create(fis);
        Sheet sheet = null;
        if (name == null)
            sheet = workbook.getSheetAt(index);
        else
            sheet = workbook.getSheet(name);

        // try {

        //     fis = new FileInputStream(filepath);
        //     // workbook = new HSSFWorkbook(fis);
        //     workbook = WorkbookFactory.create(fis);

        //     if (name == null)
        //         sheet = workbook.getSheetAt(index);
        //     else
        //         sheet = workbook.getSheet(name);

        // } catch (FileNotFoundException e) {
        //     e.printStackTrace();
        // } catch (IOException e) {
        //     e.printStackTrace();
        // } catch (InvalidFormatException e) {
        //    e.printStackTrace();
        // } finally {
        //     if (fis != null)
        //         try {
        //             fis.close();
        //         } catch (IOException e) {
        //             e.printStackTrace();
        //         }
        // }

        fis.close();

        Object[][] data = null;

        if (sheet != null) data = read(sheet);

        return data;

    }

    private static Object[][] read(Sheet sheet) {

        if (sheet == null) return null;

        int i = 0, j = 0 , k = 0;
        List<ArrayList<Object>> list = new ArrayList<ArrayList<Object>>();
        Iterator<Row>  rows = sheet.iterator();
        while (rows.hasNext()) {

            j = 0;
            List<Object> ls = new ArrayList<Object>();
            Row row = rows.next();
            Iterator<Cell> cells = row.iterator();
            while (cells.hasNext()) {
                Cell cell = cells.next();

                Object o = null;
                switch(cell.getCellType()) {
                    case Cell.CELL_TYPE_BOOLEAN:
                        o = cell.getBooleanCellValue();
                        break;
                    case Cell.CELL_TYPE_NUMERIC:
                        o = cell.getNumericCellValue();
                        break;
                    case Cell.CELL_TYPE_STRING:
                        o = cell.getStringCellValue();
                        break;
                }
                ls.add(o);
                j++;
            }
            list.add((ArrayList<Object>) ls);
            i++;
            if (k < j) k = j;
        }

        int m = 0, n = 0;
        Object[][] data = new Object[i][k];
        for (ArrayList<Object> l : list) {
            for (Object o : l) {
                data[m][n] = o;
                n++;
            }
            m++;
            n = 0;
        }

        return data;

    }

    public static void write(String filePath, Object[][] data) throws IOException {

        write(filePath, "Sheet1", data);

    }

    public static void write(String filePath, String name, Object[][] data) throws IOException {

        FileOutputStream fos = null;
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet(name);

        int rowNum = 0;
        for (Object[] line : data) {
            Row row = sheet.createRow(rowNum++);
            int colNum = 0;
            for (Object field : line) {
                Cell cell = row.createCell(colNum++);
                if (field instanceof String) {
                    cell.setCellValue((String) field);
                } else if (field instanceof Integer) {
                    cell.setCellValue((Integer) field);
                } else if (field instanceof Boolean) {
                    cell.setCellValue((Boolean) field);
                } else if (field instanceof Long) {
                    cell.setCellValue((Long) field);
                }
            }
        }

        fos = new FileOutputStream(filePath);
        workbook.write(fos);
        fos.close();

        // try {
        //     fos = new FileOutputStream(filepath);
        //     workbook.write(fos);
        // } catch (FileNotFoundException e) {
        //     e.printStackTrace();
        // } catch (IOException e) {
        //     e.printStackTrace();
        // } finally {
        //     if (fos != null)
        //         try {
        //             fos.close();
        //         } catch (IOException e) {
        //             e.printStackTrace();
        //         }
        // }

    }

}
