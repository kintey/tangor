package io.lakesidejar.io;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;

public class TextManipulator {
    
    public static final String LINE_DELIMITER = System.getProperty("line.separator");
    public static final String DEFAULT_CHARSET = "UTF-8";

    public static String read(String filePath, String charset) throws IOException {

        if (charset == null) charset = DEFAULT_CHARSET;

        FileInputStream fis = new FileInputStream(filePath);
        InputStreamReader isr = new InputStreamReader(fis, charset);
        BufferedReader br = new BufferedReader(isr);
        StringBuffer sb = new StringBuffer();

        // try {
        //     fis = new FileInputStream(filepath);
        //     isr = new InputStreamReader(fis, charset);
        //     br = new BufferedReader(isr);

        //     while ((str = br.readLine()) != null)
        //         sb.append(str + LINE_DELIMITER);

        // } catch (FileNotFoundException e) {
        //     e.printStackTrace();
        // } catch (IOException e) {
        //     e.printStackTrace();
        // } finally {
        //     if (br != null)
        //         try {
        //             br.close();
        //         } catch (IOException e) {
        //             e.printStackTrace();
        //         }
        // }

        String str;
        while ((str = br.readLine()) != null)
            sb.append(str + LINE_DELIMITER);

        br.close();

        return sb.toString();

    }

    public static String read(String filePath) throws IOException {

        return read(filePath, null);

    }

    public static long count(String filePath) throws IOException {

        long rows = 0;

        FileInputStream fis = new FileInputStream(filePath);
        InputStreamReader isr = new InputStreamReader(fis);
        BufferedReader br = new BufferedReader(isr);

        while (br.readLine() != null) rows++;

        br.close();

        return rows;

    }

    public static void write(String filePath, String content, String charset) throws IOException {

        if (charset == null) charset = DEFAULT_CHARSET;

        File file = new File(filePath);
        if (!file.getParentFile().exists())
            file.getParentFile().mkdirs();

        FileOutputStream fos = new FileOutputStream(file);
        OutputStreamWriter osw = new OutputStreamWriter(fos, charset);
        BufferedWriter bw = new BufferedWriter(osw);

        bw.write(content);
        bw.close();

        // try {
        //     fos = new FileOutputStream(filepath);
        //     osw = new OutputStreamWriter(fos, charset);
        //     bw = new BufferedWriter(osw);

        //     bw.write(content);

        // } catch (FileNotFoundException e) {
        //     e.printStackTrace();
        // } catch (IOException e) {
        //     e.printStackTrace();
        // } finally {
        //     if (bw != null)
        //         try {
        //             bw.close();
        //         } catch (IOException e) {
        //             e.printStackTrace();
        //         }
        // }

    }

    public static void write(String filePath, String content) throws IOException {

        write(filePath, content, null);

    }

}
