package io.lakesidejar.regex;

public class TextAnalysor {

    private static final String JSP_REGEX = ".jsp$";
    private static final String EXCLUDED_PATH_REGEX = "\\\\lib\\\\";

    public static boolean isDate(String str) {

        String regex;
        boolean b;

        regex = "日";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "年月";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "Dt$";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "Dt2$";
        b = Comparator.exist(str, regex);
        if (b) return b;

        return b;
    }

    public static boolean isNumeric(String str) {
        String regex;
        boolean b;

        regex = "コード$";
        b = Comparator.exist(str, regex);
        if (b) return false;

        regex = "有無";
        b = Comparator.exist(str, regex);
        if (b) return false;

        regex = "方法";
        b = Comparator.exist(str, regex);
        if (b) return false;

        regex = "Name$";
        b = Comparator.exist(str, regex);
        if (b) return false;

        regex = "Cd$";
        b = Comparator.exist(str, regex);
        if (b) return false;

        regex = "率";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "額";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "係数";
        b = Comparator.exist(str, regex);
        if (b) return b;

        // regex = "通貨";
        // b = isComparator.exist(str, regex);
        // if (b) return b;

        regex = "数";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "価";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "合計";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "料";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "Rate";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "Amount";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "Amnt";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "Amt";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "Coef";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "curren";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "fund";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "Val$";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "Value$";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "Ttl";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "prem$";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "Prem$";
        b = Comparator.exist(str, regex);
        if (b) return b;

        regex = "Ratio$";
        b = Comparator.exist(str, regex);
        if (b) return b;

        // regex = "表示$";
        // b = isComparator.exist(str, regex);
        // if (b) return false;

        // regex = "Disp$";
        // b = isComparator.exist(str, regex);
        // if (b) return false;

        // regex = "Display$";
        // b = isComparator.exist(str, regex);
        // if (b) return false;

        return b;
    }

    // public static boolean isAmount(String str) {

    //     String regex = "額";

    //     return isComparator.exist(str, regex);

    // }

    // public static boolean isRate(String str) {

    //     String regex = "率";

    //     return isComparator.exist(str, regex);

    // }

    // public static String getComment(String str) {

    //     return Comparator.getContent(str, COMMENT_REGEX).trim();

    // }

    public static boolean isJsp(String str) {

        return Comparator.exist(str, JSP_REGEX);

    }

    public static boolean isExcludedPath(String str) {

        return Comparator.exist(str, EXCLUDED_PATH_REGEX);

    }

}
