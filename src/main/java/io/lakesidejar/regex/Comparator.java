package io.lakesidejar.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Comparator {

    private static Pattern pattern;
    private static Matcher matcher;

    // public static boolean isBigDecimal(String str) {

    //     String regex = "^\\s*private\\s+BigDecimal\\s+\\w+\\s*;\\s*$";

    //     return exist(str, regex);
    // }

    // public static boolean isDate(String str) {
    //     String regex;
    //     boolean b;

    //     regex = "日";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "年月";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "Dt$";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "Dt2$";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "^\\s*private\\s+Date\\s+\\w+\\s*;\\s*$";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     return b;
    // }

    // public static boolean isNumeric(String str) {
    //     String regex;
    //     boolean b;

    //     regex = "コード$";
    //     b = exist(str, regex);
    //     if (b) return false;

    //     regex = "有無";
    //     b = exist(str, regex);
    //     if (b) return false;

    //     regex = "方法";
    //     b = exist(str, regex);
    //     if (b) return false;

    //     regex = "Name$";
    //     b = exist(str, regex);
    //     if (b) return false;

    //     regex = "Cd$";
    //     b = exist(str, regex);
    //     if (b) return false;

    //     regex = "率";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "額";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "係数";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     // regex = "通貨";
    //     // b = isExist(str, regex);
    //     // if (b) return b;

    //     regex = "数";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "価";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "合計";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "料";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "Rate";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "Amount";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "Amnt";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "Amt";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "Coef";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "curren";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "fund";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "Val$";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "Value$";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "Ttl";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "prem$";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "Prem$";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     regex = "Ratio$";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     // regex = "表示$";
    //     // b = isExist(str, regex);
    //     // if (b) return false;

    //     // regex = "Disp$";
    //     // b = isExist(str, regex);
    //     // if (b) return false;

    //     // regex = "Display$";
    //     // b = isExist(str, regex);
    //     // if (b) return false;

    //     return b;
    // }

    // public static boolean isAmount(String str) {

    //     String regex = "額";

    //     return isExist(str, regex);

    // }

    // public static boolean isRate(String str) {

    //     String regex = "率";

    //     return isExist(str, regex);

    // }

    // public static boolean isStatement(String str) {

    //     // String regex = "(?!//).*[public|private]\\sString.*;$";
    //     // String regex = "^\\s((?!//)[public|private]\\sString\\s)*;$";

    //     // return isExist(str, regex);

    //     String regex;
    //     boolean b;

    //     regex = "\\s*?//.*;";
    //     b = exist(str, regex);
    //     if (b) return false;

    //     regex = "^\\s*private\\s+boolean\\s+\\w+\\s*;\\s*$";
    //     b = exist(str, regex);
    //     if (b) return false;

    //     regex = "^\\s*private\\s+List<.*\\s+\\w+\\s*;\\s*$";
    //     b = exist(str, regex);
    //     if (b) return false;

    //     // regex = "[public|private]\\sString.*;$";
    //     regex = "^\\s*private\\s+\\w+\\s+\\w+\\s*;\\s*$";
    //     b = exist(str, regex);
    //     if (b) return b;

    //     return b;

    // }

    // public static String getMemberName(String str) {

    //     // String regex = "\\s*[public|private]\\sString\\s(.*?)\\s*;$";
    //     String regex = "^\\s*private\\s+\\w+\\s+(\\w+)\\s*;\\s*$";

    //     return getContent(str, regex);

    // }

    // public static boolean isComment(String str) {

    //     String regex = "/\\*\\*(.)*?\\*/";

    //     return exist(str, regex);

    // }

    // public static String getComment(String str) {

    //     String regex = "/\\*\\*\\s*?(.*?)\\s*?\\*/";

    //     return getContent(str, regex);

    // }

    public static boolean exist(String str, String regex) {

        pattern = Pattern.compile(regex);
        matcher = pattern.matcher(str);

        return matcher.find();

    }

    public static String getContent(String str, String regex) {

        pattern = Pattern.compile(regex);
        matcher = pattern.matcher(str);

        String s = "";

        if (matcher.find())
            s = matcher.group(1);

        return s;

    }

}
