package io.lakesidejar.regex;

public class JavaAnalysor {

    private static final String STATEMENT_REGEX = "^\\s*private\\s+.*\\s*;\\s*$";
    private static final String BIG_DECIMAL_REGEX = "^\\s*private\\s+BigDecimal\\s+\\w+\\s*;\\s*$";
    private static final String DATE_REGEX = "^\\s*private\\s+Date\\s+\\w+\\s*;\\s*$";
    private static final String COMMENT_REGEX = "^\\s*//.*\\s*$";
    private static final String BOOLEAN_REGEX = "^\\s*private\\s+boolean\\s+\\w+\\s*;\\s*$";
    private static final String LIST_REGEX = "^\\s*private\\s+List.*;\\s*$";
    private static final String NUMBER_NAME_REGEX = "^\\s*private\\s+\\w+\\s+(\\w+)\\s*;\\s*$";
    // private static final String DESCRIPTION_REGEX = "/\\*\\*(.)*?\\*/";
    private static final String DESCRIPTION_REGEX = "/\\*\\*\\s*?(.*?)\\s*?\\*/";

    public static boolean isDate(String str) {

        return Comparator.exist(str, DATE_REGEX);

    }

    public static boolean isNumeric(String str) {

        return Comparator.exist(str, BIG_DECIMAL_REGEX);

    }

    public static boolean isComment(String str) {

        return Comparator.exist(str, COMMENT_REGEX);

    }

    public static boolean isDescription(String str) {

        return Comparator.exist(str, DESCRIPTION_REGEX);

    }

    public static boolean isBoolean(String str) {

        return Comparator.exist(str, BOOLEAN_REGEX);

    }

    public static boolean isList(String str) {

        return Comparator.exist(str, LIST_REGEX);

    }

    public static boolean isStatement(String str) {

        return Comparator.exist(str, STATEMENT_REGEX);

    }

    public static String getMemberName(String str) {

        return Comparator.getContent(str, NUMBER_NAME_REGEX);

    }

    public static String getDescription(String str) {

        return Comparator.getContent(str, DESCRIPTION_REGEX).trim();

    }

}
