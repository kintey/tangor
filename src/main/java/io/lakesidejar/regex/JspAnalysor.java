package io.lakesidejar.regex;

public class JspAnalysor {

    private static final String EXPRESSION_REGEX = "\\{\\{.*?\\}\\}";
    private static final String INPUT_REGEX = "input";

    public static boolean isExpression(String str) {
        return Comparator.exist(str, EXPRESSION_REGEX);
    }

    public static boolean isInputItem(String str) {
        return Comparator.exist(str, INPUT_REGEX);
    }

}
