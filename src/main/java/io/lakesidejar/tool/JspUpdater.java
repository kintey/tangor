package io.lakesidejar.tool;

import java.io.IOException;
import java.util.List;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import io.lakesidejar.io.ExcelManipulator;
import io.lakesidejar.io.FileManipulator;
import io.lakesidejar.io.TextManipulator;
import io.lakesidejar.regex.JavaAnalysor;

public class JspUpdater {

    public static void main(String[] args) {

        String jspPath = "E:\\Documents\\98.報告\\20180607\\src\\jsp";
        String javaPath = "E:\\Documents\\98.報告\\20180607\\upt\\bean";
        String filePath = "E:\\Documents\\98.報告\\20180608\\jsp.xlsx";

        JspUpdater app = new JspUpdater();

        System.out.println("Proccess start...");
        try {
            app.start(jspPath, javaPath, filePath);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
        System.out.println("Proccess finished!");
    }

    public void start(String jspPath, String javaPath, String filePath) throws IOException, InvalidFormatException {

        List<String> jspFiles = FileManipulator.getAbsolutePaths(jspPath);
        List<String> javaFiles = FileManipulator.getAbsolutePaths(javaPath);
        Object[][] data = ExcelManipulator.read(filePath);

        for (Object[] row : data) {
            int colNum = 0;
            String jspFilePath = null;
            String content = null;
            for (Object cell : row) {
                colNum++;
                if (cell == null) continue;

                // String[] lines = null;
                if (colNum == 1) {
                    jspFilePath = getFilePath(jspFiles, cell.toString());
                    if (jspFilePath == null) break;
                    content = TextManipulator.read(jspFilePath);
                } else {
                    String javaFilePath = getFilePath(javaFiles, cell.toString());
                    if (javaFilePath == null) continue;
                    // lines = FileManipulator.read(javaFilePath).split(FileManipulator.LINE_DELIMITER);
                    // for (String line : lines) {
                    //     if (!Comparator.isStatement(line)) continue;
                    //     String var = Comparator.getMemberName(line);
                    //     if (Comparator.isBigDecimal(line))
                    //         str = str.replaceAll(var + "}}", var + " | cNumber}}");
                    //     if (Comparator.isDate(line))
                    //         str = str.replaceAll(var + "}}", var + " | date:\"yyyy/MM/dd\"}}");
                    // }
                    content = process(content, TextManipulator.read(javaFilePath));
                }
            }
            if (jspFilePath == null || content == null) continue;
            TextManipulator.write(jspFilePath.replaceFirst("src", "upt"), content);
        }

    }

    private String process(String jsp, String java) {

        if (jsp == null || java == null) return null;

        String content = jsp;
        String[] lines = java.split(TextManipulator.LINE_DELIMITER);

        for (String line : lines) {
            if (!JavaAnalysor.isStatement(line)) continue;
            String name = JavaAnalysor.getMemberName(line);
            if (JavaAnalysor.isNumeric(line))
                content = content.replaceAll(name + "}}", name + " | cNumber}}");
            if (JavaAnalysor.isDate(line))
                content = content.replaceAll(name + "}}", name + " | date:\"yyyy/MM/dd\"}}");
        }

        return content;

    }

    private String getFilePath(List<String> files, String name) {

        String path = null;

        for (String s : files) {
            if (s.indexOf(name) > 0) {
                path = s;
                break;
            }
        }

        return path;

    }

}
