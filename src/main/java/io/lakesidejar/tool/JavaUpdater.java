package io.lakesidejar.tool;

import java.io.IOException;
import java.util.List;

import io.lakesidejar.io.FileManipulator;
import io.lakesidejar.io.TextManipulator;
import io.lakesidejar.regex.Comparator;
import io.lakesidejar.regex.JavaAnalysor;
import io.lakesidejar.regex.TextAnalysor;

public class JavaUpdater {

    private static final String ANNOTATION = "@JsonSerialize(using=ToStringSerializer.class)";

    public static void main(String[] args) {

        String fullpath = "E:\\Documents\\98.報告\\20180607";

        JavaUpdater app = new JavaUpdater();

        System.out.println("Proccess start...");
        try {
            app.start(fullpath);
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Proccess finished!");
    }

    public void start(String fullPath) throws IOException {

        List<String> files = FileManipulator.getAbsolutePaths(fullPath);

        // String[] lines;
        // String filepath;
        // String content;

        for (String file : files) {
            String content = process(file);
            String filePath = file.replaceFirst("src", "upt");
            TextManipulator.write(filePath, content);
        }
    }

    private String process(String filePath) throws IOException {

        String[] lines = TextManipulator.read(filePath).split(TextManipulator.LINE_DELIMITER);
        StringBuffer sb = new StringBuffer();

        // String line;
        // boolean commentLine = false;
        // boolean statementLine;
        // String comment;
        // String memberName;

        for (int i = 0, len = lines.length; i < len; i++) {

            String line = lines[i];

            boolean statementLine = JavaAnalysor.isStatement(line);
            boolean documentLine = false;
            if (statementLine && !JavaAnalysor.isComment(line) && i > 1)
                documentLine = JavaAnalysor.isDescription(lines[i - 1]);

            if (documentLine && statementLine) {
                String description = JavaAnalysor.getDescription(lines[i - 1]);
                String memberName = JavaAnalysor.getMemberName(line);

                if (TextAnalysor.isNumeric(description) && TextAnalysor.isNumeric(memberName))
                    line = "    " + ANNOTATION + TextManipulator.LINE_DELIMITER + line.replaceFirst("String", "BigDecimal");

                if (TextAnalysor.isDate(description) && TextAnalysor.isDate(memberName)) {
                    line = line.replaceFirst("String", "Date");
                    if (Comparator.exist(line, "BigDecimal")) {
                        line = line.substring(line.indexOf(TextManipulator.LINE_DELIMITER) + 2);
                        line = line.replaceFirst("BigDecimal", "Date");
                    };
                }

            }

            sb.append(line + TextManipulator.LINE_DELIMITER);

        }

        return sb.toString();
    }

}
