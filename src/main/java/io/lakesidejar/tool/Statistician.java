package io.lakesidejar.tool;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import io.lakesidejar.io.ExcelManipulator;
import io.lakesidejar.io.FileManipulator;
import io.lakesidejar.io.TextManipulator;
import io.lakesidejar.regex.Comparator;
import io.lakesidejar.regex.JspAnalysor;
import io.lakesidejar.regex.TextAnalysor;

public class Statistician {

    private String FILE_EXTENSION = "java|jsp|js|css";
    private String EXTENSION_SEPARATOR = "\\|";

    public static void main(String[] args) {

        // String path = "E:\\Documents\\98.報告\\20180607\\src";
        String path = "C:\\Users\\Zero\\eclipse-workspace\\intra";
        String reportPath = "E:\\Documents\\98.報告\\20180611\\result.xlsx";

        Statistician app = new Statistician();
        try {
            System.out.println("app start..");
            app.countLines(path, reportPath);
            System.out.println("app finished..");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private List<String> getSources(String fullPath) {

        List<String> files = FileManipulator.getAbsolutePaths(fullPath);
        String[] extensions = FILE_EXTENSION.split(EXTENSION_SEPARATOR);

        List<String> textFiles = new ArrayList<String>();
        for (String file : files) {
            if (TextAnalysor.isExcludedPath(file)) continue;
            for (String extension : extensions) {
                String regex = "\\." + extension + "$";
                if (Comparator.exist(file, regex)) {
                    textFiles.add(file);
                    break;
                }
            }
        }

        // List<String> textFiles = new ArrayList<String>();
        // for (String file : files) {
        //     if(FileManipulator.isBinary(file))
        //         textFiles.add(file);
        // }

        return textFiles;

    }

    public void countLines(String fullPath, String reportPath) throws IOException {

        List<String> files = getSources(fullPath);

        Object[][] data = new Object[files.size()][4];
        int m = 0;
        for (String s : files) {
            data[m][0] = s;
            data[m][1] = TextManipulator.count(s);
            m++;
        }
        
        countExpression(data);
        countInputItem(data);

        ExcelManipulator.write(reportPath, data);
    }

    private void countExpression(Object[][] data) throws IOException {

        int expnNum;
        for (Object[] row : data) {
            String path = (String) row[0];
            if (TextAnalysor.isJsp(path)) {
                expnNum = 0;
                String[] lines = TextManipulator.read(path).split(TextManipulator.LINE_DELIMITER);
                for (String line : lines) {
                    if (JspAnalysor.isExpression(line))
                        expnNum++;
                }
                row[2] = expnNum;
            }
        }

    }

    private void countInputItem(Object[][] data) throws IOException {

        int inputNum;
        for (Object[] row : data) {
            String path = (String) row[0];
            if (TextAnalysor.isJsp(path)) {
                inputNum = 0;
                String[] lines = TextManipulator.read(path).split(TextManipulator.LINE_DELIMITER);
                for (String line : lines) {
                    if (JspAnalysor.isInputItem(line))
                        inputNum++;
                }
                row[3] = inputNum;
            }
        }

    }

}
