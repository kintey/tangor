package io.lakesidejar.tool;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Test;

public class JspUpdaterTest {

    @Test
    public void start() {
        String jspPath = "E:\\Documents\\98.報告\\20180607\\src\\jsp";
        String javaPath = "E:\\Documents\\98.報告\\20180607\\upt\\bean";
        String filePath = "E:\\Documents\\98.報告\\20180608\\jsp.xlsx";

        JspUpdater app = new JspUpdater();

        System.out.println("Proccess start...");
        try {
            app.start(jspPath, javaPath, filePath);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (InvalidFormatException e) {
            e.printStackTrace();
        }
        System.out.println("Proccess finished!");
    }

}
