package io.lakesidejar.regex;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TextAnalysorTest {

    // @Test
    // public void getComment() {
    //     String str = "/** 加重平均料率(STB) */";
    //     assertEquals(TextAnalysor.getComment(str), "加重平均料率(STB)");
    // }

    @Test
    public void isDate() {
        // String str = "統合日";
        String str = "cntrtResetDt";
        assertTrue(TextAnalysor.isDate(str));
    }


    @Test
    public void isNumeric() {
        // String str = "加重平均料率(STB)";
        // String str = "STB支払総額(邦貨)";
        // String str = "保険成績調整係数(STB)";
        // String str = "insuredRate";
        // String str = "XxAmount";
        // String str = "XxAmnt";
        // String str = "XxAmt";
        String str = "insuredAdjstCoef";
        assertTrue(TextAnalysor.isNumeric(str));
    }

    @Test
    public void isJsp() {
        String str = "C:\\Users\\Zero\\eclipse-workspace\\intra\\WebContent\\WEB-INF\\jsp\\accident\\DIA0207.jsp";
        assertTrue(TextAnalysor.isJsp(str));
    }

    @Test
    public void isExcludedPath() {
        String str = "C:\\Users\\Zero\\eclipse-workspace\\intra\\WebContent\\lib\\IgniteUI\\css\\structure\\jquery-ui.css";
        assertTrue(TextAnalysor.isExcludedPath(str));
    }

}
