package io.lakesidejar.regex;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class JavaAnalysorTest {

    @Test
    public void isComment() {
        String str = "    // public void isRate() {";
        assertTrue(JavaAnalysor.isComment(str));
    }

    @Test
    public void isDate() {
        // String str = "private boolean insuredAdjstCoef;";
        String str = "private Date settleDt;";
        assertTrue(JavaAnalysor.isDate(str));
    }

    @Test
    public void isNumeric() {
        // String str = "private boolean insuredAdjstCoef;";
        String str = "private BigDecimal insuredAdjstCoef;";
        assertTrue(JavaAnalysor.isNumeric(str));
    }

    @Test
    public void isDescription() {
        String str = "/** 決済・償還 */";
        assertTrue(JavaAnalysor.isDescription(str));
    }

    @Test
    public void isBoolean() {
        // String str = "private String s ;";
        String str = "private boolean aaa ;";
        assertTrue(JavaAnalysor.isBoolean(str));
    }

    @Test
    public void isList() {
        String str = "private List<Map<String, String>> interRateFormula1List = new ArrayList<>();";
        // String str = "private List< a;";
        assertTrue(JavaAnalysor.isList(str));
    }

    @Test
    public void isStatement() {
        String str = "private boolean aaa ;";
        // String str = "private List<Map<String, String>> interRateFormula1List = new ArrayList<>();";
        assertTrue(JavaAnalysor.isStatement(str));
    }

    @Test
    public void getMemberName() {
        String str = "private BigDecimal insuredAdjstCoef;";
        assertEquals(JavaAnalysor.getMemberName(str), "insuredAdjstCoef");
    }

    @Test
    public void getDescription() {
        String str = "/** 加重平均料率(STB) */";
        assertEquals(JavaAnalysor.getDescription(str), "加重平均料率(STB)");
    }

}
