package io.lakesidejar.regex;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class JspAnalysorTest {

    @Test
    public void isExpression() {
        // String str = "{{abc}}";
        String str = "{abc}";
        assertTrue(JspAnalysor.isExpression(str));
    }

    @Test
    public void isInputItem() {
        String str = "<input name=";
        assertTrue(JspAnalysor.isInputItem(str));
    }

}
