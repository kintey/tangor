package io.lakesidejar.io;

import java.io.IOException;

import org.junit.Test;

public class TextManipulatorTest {

    @Test
    public void read() throws IOException {
        
        String filepath = "E:\\Documents\\98.報告\\20180607\\src\\bean\\DIU0102ShipmntSettleForm.java";

        String str = TextManipulator.read(filepath);
        // System.out.print(str);

        String[] lines = str.split(TextManipulator.LINE_DELIMITER);
        for (String line : lines)
            System.out.println(line);

    }

    @Test
    public void write() throws IOException {

        String filepath1 = "E:\\Documents\\98.報告\\20180607\\src\\bean\\DIU0102ShipmntSettleForm.java";
        String filepath2 = "E:\\Documents\\98.報告\\20180607\\src1\\bean\\DIU0102ShipmntSettleForm.java";

        TextManipulator.write(filepath2, TextManipulator.read(filepath1));

    }

    @Test
    public void count() throws IOException {
        String str = "E:\\Documents\\98.報告\\20180607\\src\\bean\\DIU0102ShipmntSettleForm.java";
        System.out.println(TextManipulator.count(str));
    }
}
