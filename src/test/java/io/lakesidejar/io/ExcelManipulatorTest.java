package io.lakesidejar.io;

import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.junit.Test;

public class ExcelManipulatorTest {

    @Test
    public void read() throws InvalidFormatException, IOException {

        // String filepath = "E:\\Documents\\98.報告\\20180608\\test.xlsx";
        String filepath = "E:\\Documents\\98.報告\\20180608\\jsp.xlsx";

        Object[][] data = ExcelManipulator.read(filepath);

        // System.out.println(data);
        for (Object[] row : data) {
            for (Object cell : row) {
                System.out.print(cell);
                System.out.print('\t');
            }
            System.out.print('\n');
        }

    }

    @Test
    public void write() throws IOException {

        String filepath = "E:\\Documents\\98.報告\\20180608\\test_.xlsx";

        Object[][] data = {
                {"Datatype", "Type", "Size(in bytes)"},
                {"int", "Primitive", 2},
                {"float", "Primitive", 4},
                {"double", "Primitive", 8},
                {"char", "Primitive", 1},
                {"String", "Non-Primitive", "No fixed size"}
        };

        ExcelManipulator.write(filepath, "Datatypes in Java", data);

    }

}
