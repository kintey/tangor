package io.lakesidejar.io;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.List;

import org.junit.Test;

public class FileManipulatorTest {


    @Test
    public void getAbsolutePaths() {

        // String fullpath = "E:\\Documents\\98.報告\\20180607";
        // String fullpath = "E:\\Documents\\98.報告\\20180607\\src\\jsp";
        String fullpath = "E:\\Documents\\98.報告\\20180607\\src\\bean";

        List<String> files = FileManipulator.getAbsolutePaths(fullpath);

        for (String f : files)
            // System.out.println(f);
            System.out.println(f.substring(f.lastIndexOf('\\') + 1));

    }

    @Test
    public void isBinary() throws IOException {
        // String str = "E:\\Documents\\98.報告\\20180608\\jsp.xlsx";
        String str = "E:\\Documents\\98.報告\\20180607\\src\\bean\\DIU0102ShipmntSettleForm.java";
        assertTrue(FileManipulator.isBinary(str));
    }

}
